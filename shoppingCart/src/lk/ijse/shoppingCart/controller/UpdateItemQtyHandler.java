package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.dto.ItemDTO;
import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.ItemService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sandunDilhan on 6/20/2018.
 */
public class UpdateItemQtyHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            ItemService itemService= (ItemService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEM);

            int itemId=Integer.parseInt(req.getParameter("itemId"));
            int qty=Integer.parseInt(req.getParameter("qty"));

            System.out.println(itemId+"_"+qty);
            //update qty in item
            boolean isUpdated=itemService.updateQty(itemId,(qty-1));
            if(isUpdated){
                out.print("update success");
            }else{
                out.print("buy failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
