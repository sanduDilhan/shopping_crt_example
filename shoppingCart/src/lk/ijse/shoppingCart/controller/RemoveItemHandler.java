package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.ItemService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sandunDilhan on 6/18/2018.
 */
public class RemoveItemHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            ItemService itemService= (ItemService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEM);
            String desc=req.getParameter("description");
            //remove item using description
            boolean isDeleted=itemService.delete(desc);
            if(isDeleted){
                out.print("delete success");
            }else{
                out.print("delete failed");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
