package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.constants.EncryptPassword;
import lk.ijse.shoppingCart.dto.SuperAdminDTO;
import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.SuperAdminService;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public class AdminHandler extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            SuperAdminService superAdminService= (SuperAdminService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.SUPERADMIN);

            String userName=req.getParameter("userName");
            String password=req.getParameter("password");

            //encrypt admin password
            //check password and user name
            SuperAdminDTO superAdminDTO=superAdminService.confirmPassword(userName,password);
            if(superAdminDTO!=null){
                req.getSession().setAttribute("loginResult", "true");
                req.getSession().setAttribute("superAdmin", superAdminDTO);
                req.getSession().setMaxInactiveInterval(900); // 15 min
                req.setAttribute("loginResult", "true");
                resp.sendRedirect("index.jsp");
            }else{
                req.setAttribute("loginResult", "false");
                RequestDispatcher dispatcher = req.getRequestDispatcher("adminLogin.jsp");
                dispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
