package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.ItemTypeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sandunDilhan on 6/18/2018.
 */
public class RemoveItemTypeHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            ItemTypeService itemTypeService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
            String desc=req.getParameter("description");

            //remove item type
            boolean isDeleted=itemTypeService.delete(desc);
            if(isDeleted){
                out.print("remove success");
            }else{
                out.print("remove failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
