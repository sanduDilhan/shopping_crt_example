package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.constants.EncryptPassword;
import lk.ijse.shoppingCart.dao.custom.CustomerDAO;
import lk.ijse.shoppingCart.dto.CustomerDTO;
import lk.ijse.shoppingCart.dto.SuperAdminDTO;
import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.CustomerService;
import lk.ijse.shoppingCart.service.custom.SuperAdminService;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public class CustomerHandler extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            CustomerService customerService= (CustomerService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.CUSTOMER);

            String userName=req.getParameter("userName");
            String password=req.getParameter("password");
            System.out.println(userName+"_"+password);
//            //encrypt admin password
            String encryptPassword=new EncryptPassword().getMD5EncryptedValue(password);

            System.out.println(encryptPassword);

            //check password and user name
            CustomerDTO customerDTO=customerService.confirmPassword(userName,encryptPassword);
            if(customerDTO!=null){
                req.getSession().setAttribute("loginResult", "true");
                req.getSession().setAttribute("customer", customerDTO);
                req.getSession().setMaxInactiveInterval(900); // 15 min
                req.setAttribute("loginResult", "true");
                resp.sendRedirect("customer.jsp");
            }else{
                req.setAttribute("loginResult", "false");
                RequestDispatcher dispatcher = req.getRequestDispatcher("customerLogin.jsp");
                dispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
