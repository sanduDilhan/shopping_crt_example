package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.dto.ItemDTO;
import lk.ijse.shoppingCart.dto.ItemTypeDTO;
import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.ItemService;
import lk.ijse.shoppingCart.service.custom.ItemTypeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sandunDilhan on 6/18/2018.
 */
public class UpdateItemHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            ItemService itemService= (ItemService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEM);
            ItemTypeService itemTypeService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
            int itemId=Integer.parseInt(req.getParameter("itemId"));
            String itemTypeName=req.getParameter("itemTypeName");
            String desc=req.getParameter("description");
            int qty=Integer.parseInt(req.getParameter("qty"));
            double price=Double.parseDouble(req.getParameter("price"));

            //get itemType name
            ItemTypeDTO itemTypeDTO=itemTypeService.search(itemTypeName);
            //update item
            ItemDTO itemDTO=new ItemDTO(itemId,itemTypeDTO.getItemTypeId(),desc,qty,price);
            boolean isUpdated=itemService.update(itemDTO);
            if(isUpdated){
                out.print("update success");
            }else{
                out.print("item update failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
