package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.constants.EncryptPassword;
import lk.ijse.shoppingCart.dto.CustomerDTO;
import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.CustomerService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sandunDilhan on 6/18/2018.
 */
public class AddCustomerHandler extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            CustomerService customerService= (CustomerService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.CUSTOMER);
            String name=req.getParameter("customerName");
            String mobileNumber=req.getParameter("MobileNumber");
            String password=req.getParameter("createPassword");

            String encryptPassword=new EncryptPassword().getMD5EncryptedValue(password);

            System.out.println(encryptPassword);
            //add customer
            CustomerDTO customerDTO=new CustomerDTO(0,name,mobileNumber,encryptPassword);
            boolean isAdded=customerService.add(customerDTO);
            if(isAdded){
                resp.sendRedirect("manageCustomer.jsp");
            }else{
                out.print("<script>alert('customer added failed')</script>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
