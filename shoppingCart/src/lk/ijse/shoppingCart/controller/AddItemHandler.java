package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.dto.ItemDTO;
import lk.ijse.shoppingCart.dto.ItemTypeDTO;
import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.ItemService;
import lk.ijse.shoppingCart.service.custom.ItemTypeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sandunDilhan on 6/18/2018.
 */
public class AddItemHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            ItemService itemService= (ItemService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEM);
            ItemTypeService itemTypeService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
            String itemTypeName=req.getParameter("itemTypeName");
            String desc=req.getParameter("description");
            int qty=Integer.parseInt(req.getParameter("qty"));
            double price=Double.parseDouble(req.getParameter("price"));

            //get itemType name
            ItemTypeDTO itemTypeDTO=itemTypeService.search(itemTypeName);
            //add item
            ItemDTO itemDTO=new ItemDTO(0,itemTypeDTO.getItemTypeId(),desc,qty,price);
            boolean isAdded=itemService.add(itemDTO);
            if(isAdded){
                resp.sendRedirect("manageItem.jsp");
            }else{
                out.print("<script>alert('item added failed')</script>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
