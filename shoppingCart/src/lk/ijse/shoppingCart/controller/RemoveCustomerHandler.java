package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.CustomerService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sandunDilhan on 6/18/2018.
 */
public class RemoveCustomerHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            CustomerService customerService= (CustomerService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.CUSTOMER);
            String customerName=req.getParameter("customerName");

            //Remove Customer using customerName
            boolean isDeleted=customerService.delete(customerName);
            if(isDeleted){
                out.print("customer remove success");
            }else{
                out.print("customer remove failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
