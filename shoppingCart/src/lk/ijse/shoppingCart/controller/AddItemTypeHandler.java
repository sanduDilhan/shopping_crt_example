package lk.ijse.shoppingCart.controller;

import lk.ijse.shoppingCart.dto.ItemTypeDTO;
import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.ItemTypeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sandunDilhan on 6/18/2018.
 */
public class AddItemTypeHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            ItemTypeService itemTypeService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
            String name=req.getParameter("itemType");

            //add itemType
            ItemTypeDTO itemTypeDTO=new ItemTypeDTO(0,name);
            boolean isAdded=itemTypeService.add(itemTypeDTO);
            if(isAdded){
                resp.sendRedirect("manageItemType.jsp");
            }else{
                out.print("<script>alert('Item Type added failed')</script>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
