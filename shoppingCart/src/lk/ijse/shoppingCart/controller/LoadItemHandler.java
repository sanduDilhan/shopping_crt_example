package lk.ijse.shoppingCart.controller;

import com.google.gson.Gson;
import lk.ijse.shoppingCart.dto.ItemDTO;
import lk.ijse.shoppingCart.dto.ItemTypeDTO;
import lk.ijse.shoppingCart.service.ServiceFactory;
import lk.ijse.shoppingCart.service.custom.ItemService;
import lk.ijse.shoppingCart.service.custom.ItemTypeService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by sandunDilhan on 6/19/2018.
 */
public class LoadItemHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out = resp.getWriter()) {
            ItemService itemService= (ItemService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEM);
            ItemTypeService itemTypeService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
            String itemTypeName=req.getParameter("itemTypeName");


            //get itemType name
            ItemTypeDTO itemTypeDTO=itemTypeService.search(itemTypeName);
            if(itemTypeDTO!=null){
                //get all item using item type
                List<ItemDTO>itemDTOList=itemService.findByItemType(itemTypeDTO.getItemTypeId());
                resp.setContentType("application/json;charset=UTF-8");
                Gson gson = new Gson();
                out.print(gson.toJson(itemDTOList));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
