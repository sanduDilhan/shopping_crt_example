package lk.ijse.shoppingCart.constants;

/**
 * Created by sandunDilhan on 6/15/2018.
 */
public class DBConstans {
    public static final String CUSTOMER="customer";
    public static final String ITEM="item";
    public static final String ITEM_TYPE="itemtype";
    public static final String SUPER_ADMIN="superadmin";

    public static final String CUSTOMER_ID="id";
    public static final String CUSTOMER_NAME="name";
    public static final String CUSTOMER_MOBILE_NUMBER="mobileNumber";
    public static final String CUSTOMER_PASSWORD="password";

    public static final String ITEM_ID="id";
    public static final String ITEM_TYPE_ID="itemTypeId";
    public static final String ITEM_DESC="itemDesc";
    public static final String ITEM_QTY="qty";
    public static final String ITEM_PRICE="price";

    public static final String ITEM_TYPEID="id";
    public static final String ITEM_TYPE_NAME="itemName";

    public static final String SUPER_ADMIN_NAME="name";
    public static final String SUPER_ADMIN_PASSWORD="password";
}
