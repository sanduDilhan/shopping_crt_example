package lk.ijse.shoppingCart.resource;

/**
 * Created by sandunDilhan on 6/15/2018.
 */
public class ResourceConnectionFactory {

    public ResourceConnection getResourceConnectionFactory()throws Exception{
        return new DatabaseResourceConnection();
    }
}
