package lk.ijse.shoppingCart.resource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class DatabaseResourceConnection implements ResourceConnection {
    @Override
    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection=null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/shoppingcart","root","sandun");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DatabaseResourceConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseResourceConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return connection;
    }
}
