package lk.ijse.shoppingCart.resource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public interface ResourceConnection {

    public Connection getConnection()throws ClassNotFoundException,SQLException;
}
