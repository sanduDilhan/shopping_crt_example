package lk.ijse.shoppingCart.dto;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class CustomerDTO extends SuperDTO{
    private int customerId;
    private String customerName;
    private String mobileNumber;
    private String password;

    public CustomerDTO() {
    }

    public CustomerDTO(String customerName, String mobileNumber, String password) {
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.password = password;
    }

    public CustomerDTO(int customerId, String customerName, String mobileNumber, String password) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.password = password;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "ItemDTO{" +
                "customerId=" + customerId +
                ", customerName='" + customerName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
