package lk.ijse.shoppingCart.dto;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class ItemDTO extends SuperDTO{
    private int itemId;
    private int itemTypeId;
    private String itemDesc;
    private int qty;
    private double price;

    public ItemDTO() {
    }

    public ItemDTO(int itemTypeId, String itemDesc, int qty, double price) {
        this.itemTypeId = itemTypeId;
        this.itemDesc = itemDesc;
        this.qty = qty;
        this.price = price;
    }

    public ItemDTO(int itemId, int itemTypeId, String itemDesc, int qty, double price) {
        this.itemId = itemId;
        this.itemTypeId = itemTypeId;
        this.itemDesc = itemDesc;
        this.qty = qty;
        this.price = price;
    }

    public ItemDTO(int itemId, int qty) {
        this.itemId = itemId;
        this.qty = qty;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(int itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ItemDTO{" +
                "itemId=" + itemId +
                ", itemTypeId=" + itemTypeId +
                ", itemDesc='" + itemDesc + '\'' +
                ", qty=" + qty +
                ", price=" + price +
                '}';
    }
}
