package lk.ijse.shoppingCart.dto;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class ItemTypeDTO extends SuperDTO{
    private int itemTypeId;
    private String itemName;

    public ItemTypeDTO() {
    }

    public ItemTypeDTO(String itemName) {
        this.itemName = itemName;
    }

    public ItemTypeDTO(int itemTypeId, String itemName) {
        this.itemTypeId = itemTypeId;
        this.itemName = itemName;
    }

    public int getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(int itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public String toString() {
        return "ItemTypeDTO{" +
                "itemTypeId=" + itemTypeId +
                ", itemName='" + itemName + '\'' +
                '}';
    }
}
