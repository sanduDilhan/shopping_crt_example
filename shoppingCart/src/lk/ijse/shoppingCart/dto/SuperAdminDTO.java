package lk.ijse.shoppingCart.dto;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class SuperAdminDTO extends SuperDTO{
    private String name;
    private String password;

    public SuperAdminDTO() {
    }

    public SuperAdminDTO(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "SuperAdminDTO{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
