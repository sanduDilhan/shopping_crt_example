package lk.ijse.shoppingCart.model;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class ItemType extends SuperModel{
    private int itemTypeId;
    private String itemName;

    public ItemType() {
    }

    public ItemType(String itemName) {
        this.itemName = itemName;
    }

    public ItemType(int itemTypeId, String itemName) {
        this.itemTypeId = itemTypeId;
        this.itemName = itemName;
    }

    public int getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(int itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
