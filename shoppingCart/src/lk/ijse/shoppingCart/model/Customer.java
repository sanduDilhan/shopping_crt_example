package lk.ijse.shoppingCart.model;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class Customer extends SuperModel{
    private int customerId;
    private String customerName;
    private String mobileNumber;
    private String password;

    public Customer() {
    }

    public Customer(String customerName, String mobileNumber, String password) {
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.password = password;
    }

    public Customer(int customerId, String customerName, String mobileNumber, String password) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.password = password;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
