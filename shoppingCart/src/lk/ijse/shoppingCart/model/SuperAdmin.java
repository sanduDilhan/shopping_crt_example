package lk.ijse.shoppingCart.model;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class SuperAdmin extends SuperModel{
    private String name;
    private String password;

    public SuperAdmin() {
    }

    public SuperAdmin(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
