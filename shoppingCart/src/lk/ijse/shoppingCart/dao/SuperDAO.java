package lk.ijse.shoppingCart.dao;

import lk.ijse.shoppingCart.model.SuperModel;

import java.util.List;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public interface SuperDAO<T extends SuperModel,ID> {

    public boolean add(T model)throws Exception;

    public boolean update(T model)throws Exception;

    public boolean delete(ID id)throws Exception;

    public T search(ID id)throws Exception;

    public List<T> findAll()throws Exception;
}
