package lk.ijse.shoppingCart.dao;

import lk.ijse.shoppingCart.dao.custom.SuperAdminDAO;
import lk.ijse.shoppingCart.dao.custom.impl.CustomerDAOImpl;
import lk.ijse.shoppingCart.dao.custom.impl.ItemDAOImpl;
import lk.ijse.shoppingCart.dao.custom.impl.ItemTypeDAOImpl;
import lk.ijse.shoppingCart.dao.custom.impl.SuperAdminDAOImpl;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class DAOFactory {
    private static DAOFactory daoFactory;
    private DAOFactory(){

    }

    public enum DAOType{
        CUSTOMER,ITEM,ITEMTYPE,SUPERADMIN
    }

    public static DAOFactory getInstance(){
        if(daoFactory==null){
            daoFactory=new DAOFactory();
        }
        return daoFactory;
    }

    public SuperDAO getDAOFactory(DAOType type) throws Exception {
        switch (type){
            case CUSTOMER:return new CustomerDAOImpl();
            case ITEM:return new ItemDAOImpl();
            case ITEMTYPE:return new ItemTypeDAOImpl() ;
            case SUPERADMIN:return new SuperAdminDAOImpl();
                default:return null;

        }
    }
}
