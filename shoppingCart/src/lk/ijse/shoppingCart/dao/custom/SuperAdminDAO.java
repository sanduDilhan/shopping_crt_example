package lk.ijse.shoppingCart.dao.custom;

import lk.ijse.shoppingCart.dao.SuperDAO;
import lk.ijse.shoppingCart.model.SuperAdmin;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public interface SuperAdminDAO extends SuperDAO<SuperAdmin,String> {

    public SuperAdmin confirmPassword(String name, String password)throws Exception;
}
