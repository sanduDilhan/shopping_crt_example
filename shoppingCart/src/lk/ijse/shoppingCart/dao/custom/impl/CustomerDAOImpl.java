package lk.ijse.shoppingCart.dao.custom.impl;

import lk.ijse.shoppingCart.constants.DBConstans;
import lk.ijse.shoppingCart.dao.custom.CustomerDAO;
import lk.ijse.shoppingCart.model.Customer;
import lk.ijse.shoppingCart.resource.ResourceConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class CustomerDAOImpl implements CustomerDAO {

    private Connection connection;

    public CustomerDAOImpl() throws Exception {
        connection= new ResourceConnectionFactory().getResourceConnectionFactory().getConnection();
    }

    @Override
    public boolean add(Customer customer) throws Exception {
        String sql="insert into "+ DBConstans.CUSTOMER+" values(?,?,?,?)";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,customer.getCustomerId());
        pstm.setObject(2,customer.getCustomerName());
        pstm.setObject(3,customer.getMobileNumber());
        pstm.setObject(4,customer.getPassword());
        return pstm.executeUpdate()>0;
    }

    @Override
    public boolean update(Customer customer) throws Exception {
        String sql="update "+DBConstans.CUSTOMER+" set "+DBConstans.CUSTOMER_NAME+"=?,"+DBConstans.CUSTOMER_MOBILE_NUMBER+"=?,"+DBConstans.CUSTOMER_PASSWORD+"=? where "+DBConstans.CUSTOMER_ID+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,customer.getCustomerName());
        pstm.setObject(2,customer.getMobileNumber());
        pstm.setObject(3,customer.getPassword());
        pstm.setObject(4,customer.getCustomerId());
        return pstm.executeUpdate()>0;
    }

    @Override
    public boolean delete(String customerName) throws Exception {
        String sql="delete from "+DBConstans.CUSTOMER+" where "+DBConstans.CUSTOMER_NAME+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,customerName);
        return pstm.executeUpdate()>0;
    }

    @Override
    public Customer search(String customerName) throws Exception {
        String sql="select * from "+DBConstans.CUSTOMER+" where "+DBConstans.CUSTOMER_NAME+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,customerName);
        ResultSet rst=pstm.executeQuery();
        if(rst.next()){
            Customer customer=new Customer(rst.getInt(DBConstans.CUSTOMER_ID),rst.getString(DBConstans.CUSTOMER_NAME),rst.getString(DBConstans.CUSTOMER_MOBILE_NUMBER),rst.getString(DBConstans.CUSTOMER_PASSWORD));
            return customer;
        }
        return null;
    }

    @Override
    public List<Customer> findAll() throws Exception {
        String sql="select * from "+DBConstans.CUSTOMER+"";
        PreparedStatement pstm=connection.prepareStatement(sql);
        ResultSet rst=pstm.executeQuery();
        List<Customer>customerList=new ArrayList<>();

        while (rst.next()){
            Customer customer=new Customer(rst.getInt(DBConstans.CUSTOMER_ID),rst.getString(DBConstans.CUSTOMER_NAME),rst.getString(DBConstans.CUSTOMER_MOBILE_NUMBER),rst.getString(DBConstans.CUSTOMER_PASSWORD));
            customerList.add(customer);
        }
        return customerList;
    }

    @Override
    public Customer confirmPassword(String name, String password) throws Exception {
        String sql="select * from "+DBConstans.CUSTOMER+" where "+DBConstans.CUSTOMER_NAME+"=? AND "+DBConstans.CUSTOMER_PASSWORD+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,name);
        pstm.setObject(2,password);
        ResultSet rst=pstm.executeQuery();
        if(rst.next()){
            Customer customer=new Customer(rst.getInt(DBConstans.CUSTOMER_ID),rst.getString(DBConstans.CUSTOMER_NAME),rst.getString(DBConstans.CUSTOMER_MOBILE_NUMBER),rst.getString(DBConstans.CUSTOMER_PASSWORD));
            return customer;
        }
        return null;
    }
}
