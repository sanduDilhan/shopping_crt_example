package lk.ijse.shoppingCart.dao.custom.impl;

import lk.ijse.shoppingCart.constants.DBConstans;
import lk.ijse.shoppingCart.dao.custom.SuperAdminDAO;
import lk.ijse.shoppingCart.model.Customer;
import lk.ijse.shoppingCart.model.SuperAdmin;
import lk.ijse.shoppingCart.resource.DatabaseResourceConnection;
import lk.ijse.shoppingCart.resource.ResourceConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class SuperAdminDAOImpl implements SuperAdminDAO {
    private Connection connection;

    public SuperAdminDAOImpl() throws Exception {
        connection = new ResourceConnectionFactory().getResourceConnectionFactory().getConnection();
    }


    @Override
    public boolean add(SuperAdmin dto) throws Exception {
        return false;
    }

    @Override
    public boolean update(SuperAdmin dto) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String s) throws Exception {
        return false;
    }

    @Override
    public SuperAdmin search(String s) throws Exception {
        return null;
    }

    @Override
    public List<SuperAdmin> findAll() throws Exception {
        return null;
    }

    //confirm supper admin login.jsp
    @Override
    public SuperAdmin confirmPassword(String name, String password) throws Exception {
        String sql = "select * from " + DBConstans.SUPER_ADMIN + " where " + DBConstans.SUPER_ADMIN_NAME + "=? AND " + DBConstans.SUPER_ADMIN_PASSWORD + "=?";
        PreparedStatement pstm = connection.prepareStatement(sql);
        pstm.setObject(1, name);
        pstm.setObject(2, password);
        ResultSet rst = pstm.executeQuery();
        if (rst.next()) {
            SuperAdmin superAdmin = new SuperAdmin(rst.getString(DBConstans.SUPER_ADMIN_NAME), rst.getString(DBConstans.SUPER_ADMIN_PASSWORD));
            return superAdmin;
        }
        return null;
    }
}
