package lk.ijse.shoppingCart.dao.custom.impl;

import lk.ijse.shoppingCart.constants.DBConstans;
import lk.ijse.shoppingCart.dao.custom.ItemDAO;
import lk.ijse.shoppingCart.model.Item;
import lk.ijse.shoppingCart.resource.ResourceConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class ItemDAOImpl implements ItemDAO {

    private Connection connection;

    public ItemDAOImpl() throws Exception {
        connection=new ResourceConnectionFactory().getResourceConnectionFactory().getConnection();
    }

    @Override
    public boolean add(Item item) throws Exception {
        String sql="insert into "+ DBConstans.ITEM+" values(?,?,?,?,?)";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,item.getItemId());
        pstm.setObject(2,item.getItemTypeId());
        pstm.setObject(3,item.getItemDesc());
        pstm.setObject(4,item.getQty());
        pstm.setObject(5,item.getPrice());
        return pstm.executeUpdate()>0;

    }

    @Override
    public boolean update(Item item) throws Exception {
        String sql="update "+DBConstans.ITEM+" set "+DBConstans.ITEM_TYPE_ID+"=?,"+DBConstans.ITEM_DESC+"=?,"+DBConstans.ITEM_QTY+"=?,"+DBConstans.ITEM_PRICE+"=? where "+DBConstans.ITEM_ID+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,item.getItemTypeId());
        pstm.setObject(2,item.getItemDesc());
        pstm.setObject(3,item.getQty());
        pstm.setObject(4,item.getPrice());
        pstm.setObject(5,item.getItemId());
        return pstm.executeUpdate()>0;
    }

    @Override
    public boolean delete(String desc) throws Exception {
        String sql="delete from "+DBConstans.ITEM+" where "+DBConstans.ITEM_DESC+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,desc);
        return pstm.executeUpdate()>0;
    }

    @Override
    public Item search(String desc) throws Exception {
        String sql="select * from "+DBConstans.ITEM+" where "+DBConstans.ITEM_DESC+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,desc);
        ResultSet rst=pstm.executeQuery();
        if(rst.next()){
            Item item=new Item(rst.getInt(DBConstans.ITEM_ID),rst.getInt(DBConstans.ITEM_TYPE_ID),rst.getString(DBConstans.ITEM_DESC),rst.getInt(DBConstans.ITEM_QTY),rst.getDouble(DBConstans.ITEM_PRICE));
            return item;
        }
        return null;
    }

    @Override
    public List<Item> findAll() throws Exception {
        String sql="select * from "+DBConstans.ITEM+"";
        PreparedStatement pstm=connection.prepareStatement(sql);
        List<Item>itemList=new ArrayList<>();
        ResultSet rst=pstm.executeQuery();
        while(rst.next()){
            Item item=new Item(rst.getInt(DBConstans.ITEM_ID),rst.getInt(DBConstans.ITEM_TYPE_ID),rst.getString(DBConstans.ITEM_DESC),rst.getInt(DBConstans.ITEM_QTY),rst.getDouble(DBConstans.ITEM_PRICE));
            itemList.add(item);
        }
        return itemList;
    }

    //find by item using itemTypeId
    @Override
    public List<Item> findByItemType(int itemTypeId) throws Exception {
        String sql="select * from "+DBConstans.ITEM+" where "+DBConstans.ITEM_TYPE_ID+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,itemTypeId);
        List<Item>itemList=new ArrayList<>();
        ResultSet rst=pstm.executeQuery();
        while(rst.next()){
            Item item=new Item(rst.getInt(DBConstans.ITEM_ID),rst.getInt(DBConstans.ITEM_TYPE_ID),rst.getString(DBConstans.ITEM_DESC),rst.getInt(DBConstans.ITEM_QTY),rst.getDouble(DBConstans.ITEM_PRICE));
            itemList.add(item);
        }
        return itemList;
    }

    @Override
    public boolean updateQty(int itemTypeId,int qty) throws Exception {
        String sql="update "+DBConstans.ITEM+" set "+DBConstans.ITEM_QTY+"=? where "+DBConstans.ITEM_ID+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,qty);
        pstm.setObject(2,itemTypeId);
        return pstm.executeUpdate()>0;
    }
}
