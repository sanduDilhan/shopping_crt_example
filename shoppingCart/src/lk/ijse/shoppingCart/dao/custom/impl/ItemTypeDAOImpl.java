package lk.ijse.shoppingCart.dao.custom.impl;

import lk.ijse.shoppingCart.constants.DBConstans;
import lk.ijse.shoppingCart.dao.custom.ItemTypeDAO;
import lk.ijse.shoppingCart.model.Item;
import lk.ijse.shoppingCart.model.ItemType;
import lk.ijse.shoppingCart.resource.ResourceConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public class ItemTypeDAOImpl implements ItemTypeDAO {
    private Connection connection;

    public ItemTypeDAOImpl() throws Exception {
        connection=new ResourceConnectionFactory().getResourceConnectionFactory().getConnection();
    }

    @Override
    public boolean add(ItemType itemType) throws Exception {
        String sql="insert into "+DBConstans.ITEM_TYPE+" values(?,?)";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,itemType.getItemTypeId());
        pstm.setObject(2,itemType.getItemName());
        return pstm.executeUpdate()>0;
    }

    @Override
    public boolean update(ItemType itemType) throws Exception {
        String sql="update "+DBConstans.ITEM_TYPE+" set "+DBConstans.ITEM_TYPE_NAME+"=? where "+DBConstans.ITEM_TYPEID+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,itemType.getItemName());
        pstm.setObject(2,itemType.getItemTypeId());
        return pstm.executeUpdate()>0;
    }

    @Override
    public boolean delete(String desc) throws Exception {
        String sql="delete from "+DBConstans.ITEM_TYPE+" where "+DBConstans.ITEM_TYPE_NAME+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,desc);
        return pstm.executeUpdate()>0;
    }

    @Override
    public ItemType search(String name) throws Exception {
        String sql="select * from "+DBConstans.ITEM_TYPE+" where "+DBConstans.ITEM_TYPE_NAME+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,name);
        ResultSet rst=pstm.executeQuery();
        if(rst.next()){
            ItemType itemType=new ItemType(rst.getInt(DBConstans.ITEM_TYPEID),rst.getString(DBConstans.ITEM_TYPE_NAME));
            return itemType;
        }
        return null;
    }

    @Override
    public List<ItemType> findAll() throws Exception {
        String sql="select * from "+DBConstans.ITEM_TYPE+" ";
        PreparedStatement pstm=connection.prepareStatement(sql);
        ResultSet rst=pstm.executeQuery();
        List<ItemType>itemTypeList=new ArrayList<>();
        while(rst.next()){
            ItemType itemType=new ItemType(rst.getInt(DBConstans.ITEM_TYPEID),rst.getString(DBConstans.ITEM_TYPE_NAME));
            itemTypeList.add(itemType);
        }
        return itemTypeList;
    }

    @Override
    public ItemType findByItemType(int id) throws Exception {
        String sql="select * from "+DBConstans.ITEM_TYPE+" where "+DBConstans.ITEM_TYPEID+"=?";
        PreparedStatement pstm=connection.prepareStatement(sql);
        pstm.setObject(1,id);
        ResultSet rst=pstm.executeQuery();
        if(rst.next()){
            ItemType itemType=new ItemType(rst.getInt(DBConstans.ITEM_TYPEID),rst.getString(DBConstans.ITEM_TYPE_NAME));
            return itemType;
        }
        return null;
    }
}
