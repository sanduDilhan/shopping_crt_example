package lk.ijse.shoppingCart.dao.custom;

import lk.ijse.shoppingCart.dao.SuperDAO;
import lk.ijse.shoppingCart.dto.ItemTypeDTO;
import lk.ijse.shoppingCart.model.ItemType;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public interface ItemTypeDAO extends SuperDAO<ItemType,String> {

    public ItemType findByItemType(int id)throws Exception;
}
