package lk.ijse.shoppingCart.dao.custom;

import lk.ijse.shoppingCart.dao.SuperDAO;
import lk.ijse.shoppingCart.model.Customer;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public interface CustomerDAO extends SuperDAO<Customer,String> {
    public Customer confirmPassword(String name,String password)throws Exception;
}
