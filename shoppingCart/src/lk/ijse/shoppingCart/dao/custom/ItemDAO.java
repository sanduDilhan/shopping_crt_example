package lk.ijse.shoppingCart.dao.custom;

import lk.ijse.shoppingCart.dao.SuperDAO;
import lk.ijse.shoppingCart.model.Item;

import java.util.List;

/**
 * Created by sandunDilhan on 6/13/2018.
 */
public interface ItemDAO extends SuperDAO<Item,String> {

    public List<Item> findByItemType(int itemTypeId)throws Exception;

    public boolean updateQty(int itemTypeId,int qty)throws Exception;
}
