package lk.ijse.shoppingCart.service;

import lk.ijse.shoppingCart.service.custom.impl.CustomerServiceImpl;
import lk.ijse.shoppingCart.service.custom.impl.ItemServiceImpl;
import lk.ijse.shoppingCart.service.custom.impl.ItemTypeServiceImpl;
import lk.ijse.shoppingCart.service.custom.impl.SuperAdminServiceImpl;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public class ServiceFactory {
    private static ServiceFactory serviceFactory;

    private ServiceFactory(){

    }

    public enum ServiceType{
        CUSTOMER,ITEM,ITEMTYPE,SUPERADMIN
    }

    public static ServiceFactory getInstance(){
        if(serviceFactory==null){
            serviceFactory=new ServiceFactory();
        }
        return serviceFactory;
    }

    public SuperService getServiceFactory(ServiceType type) throws Exception {
        switch (type){
            case CUSTOMER:return new CustomerServiceImpl();
            case ITEM:return  new ItemServiceImpl();
            case ITEMTYPE: return new ItemTypeServiceImpl();
            case SUPERADMIN:return new SuperAdminServiceImpl();
            default:return null;
        }
    }
}
