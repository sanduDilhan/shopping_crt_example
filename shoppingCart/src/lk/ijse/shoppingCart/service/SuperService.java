package lk.ijse.shoppingCart.service;

import lk.ijse.shoppingCart.dto.SuperDTO;

import java.util.List;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public interface SuperService<T extends SuperDTO,ID> {
    public boolean add(T dto)throws Exception;

    public boolean update(T dto)throws Exception;

    public boolean delete(ID id)throws Exception;

    public T search(ID id)throws Exception;

    public List<T> findAll()throws Exception;
}
