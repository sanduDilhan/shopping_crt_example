package lk.ijse.shoppingCart.service.custom;

import lk.ijse.shoppingCart.dto.CustomerDTO;
import lk.ijse.shoppingCart.service.SuperService;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public interface CustomerService extends SuperService<CustomerDTO,String> {
    public CustomerDTO confirmPassword(String name,String password)throws Exception;

}
