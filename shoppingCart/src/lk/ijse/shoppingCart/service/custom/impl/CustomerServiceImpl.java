package lk.ijse.shoppingCart.service.custom.impl;

import lk.ijse.shoppingCart.dao.DAOFactory;
import lk.ijse.shoppingCart.dao.custom.CustomerDAO;
import lk.ijse.shoppingCart.dto.CustomerDTO;
import lk.ijse.shoppingCart.model.Customer;
import lk.ijse.shoppingCart.service.custom.CustomerService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public class CustomerServiceImpl implements CustomerService {
    private CustomerDAO customerDAO;

    public CustomerServiceImpl() throws Exception {
        customerDAO= (CustomerDAO) DAOFactory.getInstance().getDAOFactory(DAOFactory.DAOType.CUSTOMER);
    }

    @Override
    public boolean add(CustomerDTO customerDto) throws Exception {
        Customer customer=new Customer();
        customer.setCustomerId(customerDto.getCustomerId());
        customer.setCustomerName(customerDto.getCustomerName());
        customer.setMobileNumber(customerDto.getMobileNumber());
        customer.setPassword(customerDto.getPassword());
        boolean isAdded=customerDAO.add(customer);
        return isAdded;
    }

    @Override
    public boolean update(CustomerDTO customerDto) throws Exception {
        Customer customer=new Customer();
        customer.setCustomerId(customerDto.getCustomerId());
        customer.setCustomerName(customerDto.getCustomerName());
        customer.setMobileNumber(customerDto.getMobileNumber());
        customer.setPassword(customerDto.getPassword());
        boolean isUpdated=customerDAO.update(customer);
        return isUpdated;
    }

    @Override
    public boolean delete(String name) throws Exception {
        boolean isDeleted=customerDAO.delete(name);
        return isDeleted;
    }

    @Override
    public CustomerDTO search(String name) throws Exception {
        Customer customer=customerDAO.search(name);
        if(customer!=null){
            CustomerDTO customerDTO=new CustomerDTO();
            customer.setCustomerId(customerDTO.getCustomerId());
            customer.setCustomerName(customerDTO.getCustomerName());
            customer.setMobileNumber(customerDTO.getMobileNumber());
            customer.setPassword(customerDTO.getPassword());
            return customerDTO;
        }
        return null;
    }

    @Override
    public List<CustomerDTO> findAll() throws Exception {
        List<Customer>customerList=customerDAO.findAll();
        if(customerList!=null){
            List<CustomerDTO>customerDtoList=new ArrayList<>();
            for(Customer cusList:customerList){
                CustomerDTO customerDTO=new CustomerDTO(cusList.getCustomerId(),cusList.getCustomerName(),cusList.getMobileNumber(),cusList.getPassword());
                customerDtoList.add(customerDTO);
            }
            return customerDtoList;
        }
        return null;
    }

    @Override
    public CustomerDTO confirmPassword(String name, String password) throws Exception {
        Customer customer=customerDAO.confirmPassword(name, password);
        if(customer!=null){
            CustomerDTO customerDTO=new CustomerDTO();
            customer.setCustomerId(customerDTO.getCustomerId());
            customer.setCustomerName(customerDTO.getCustomerName());
            customer.setMobileNumber(customerDTO.getMobileNumber());
            customer.setPassword(customerDTO.getPassword());
            return customerDTO;
        }
        return null;
    }
}
