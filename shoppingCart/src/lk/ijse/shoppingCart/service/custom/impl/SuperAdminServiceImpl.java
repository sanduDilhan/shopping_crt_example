package lk.ijse.shoppingCart.service.custom.impl;

import lk.ijse.shoppingCart.dao.DAOFactory;
import lk.ijse.shoppingCart.dao.custom.SuperAdminDAO;
import lk.ijse.shoppingCart.dto.SuperAdminDTO;
import lk.ijse.shoppingCart.model.SuperAdmin;
import lk.ijse.shoppingCart.service.custom.SuperAdminService;

import java.util.List;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public class SuperAdminServiceImpl implements SuperAdminService {
    private SuperAdminDAO superAdminDAO;

    public SuperAdminServiceImpl() throws Exception {
        superAdminDAO= (SuperAdminDAO) DAOFactory.getInstance().getDAOFactory(DAOFactory.DAOType.SUPERADMIN);
    }

    @Override
    public boolean add(SuperAdminDTO dto) throws Exception {
        return false;
    }

    @Override
    public boolean update(SuperAdminDTO dto) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String s) throws Exception {
        return false;
    }

    @Override
    public SuperAdminDTO search(String s) throws Exception {
        return null;
    }

    @Override
    public List<SuperAdminDTO> findAll() throws Exception {
        return null;
    }

    @Override
    public SuperAdminDTO confirmPassword(String name, String password) throws Exception {
        SuperAdmin superAdmin=superAdminDAO.confirmPassword(name, password);
        if(superAdmin!=null){
            SuperAdminDTO superAdminDTO=new SuperAdminDTO();
            superAdmin.setName(superAdminDTO.getName());
            superAdmin.setPassword(superAdminDTO.getPassword());
            return superAdminDTO;
        }
        return  null;
    }
}
