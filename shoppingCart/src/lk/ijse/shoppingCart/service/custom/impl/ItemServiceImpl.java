package lk.ijse.shoppingCart.service.custom.impl;

import lk.ijse.shoppingCart.dao.DAOFactory;
import lk.ijse.shoppingCart.dao.custom.ItemDAO;
import lk.ijse.shoppingCart.dto.ItemDTO;
import lk.ijse.shoppingCart.model.Item;
import lk.ijse.shoppingCart.service.custom.ItemService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public class ItemServiceImpl implements ItemService {
    private ItemDAO itemDAO;

    public ItemServiceImpl() throws Exception {
        itemDAO= (ItemDAO) DAOFactory.getInstance().getDAOFactory(DAOFactory.DAOType.ITEM);
    }

    @Override
    public boolean add(ItemDTO itemDto) throws Exception {
        Item item=new Item();
        item.setItemId(itemDto.getItemId());
        item.setItemTypeId(itemDto.getItemTypeId());
        item.setItemDesc(itemDto.getItemDesc());
        item.setQty(itemDto.getQty());
        item.setPrice(itemDto.getPrice());
        return itemDAO.add(item);
    }

    @Override
    public boolean update(ItemDTO itemDto) throws Exception {
        Item item=new Item();
        item.setItemId(itemDto.getItemId());
        item.setItemTypeId(itemDto.getItemTypeId());
        item.setItemDesc(itemDto.getItemDesc());
        item.setQty(itemDto.getQty());
        item.setPrice(itemDto.getPrice());
        return itemDAO.update(item);
    }

    @Override
    public boolean delete(String desc) throws Exception {
        return itemDAO.delete(desc);
    }

    @Override
    public ItemDTO search(String desc) throws Exception {
        Item item=itemDAO.search(desc);
        if(item!=null){
            ItemDTO itemDto=new ItemDTO();
            item.setItemId(itemDto.getItemId());
            item.setItemTypeId(itemDto.getItemTypeId());
            item.setItemDesc(itemDto.getItemDesc());
            item.setQty(itemDto.getQty());
            item.setPrice(itemDto.getPrice());

            return itemDto;
        }
        return null;
    }

    @Override
    public List<ItemDTO> findAll() throws Exception {
        List<Item>itemList=itemDAO.findAll();
        if(itemList!=null){
            List<ItemDTO>itemDTOList=new ArrayList<>();
            for(Item item:itemList){
                ItemDTO itemDTO=new ItemDTO(item.getItemId(),item.getItemTypeId(),item.getItemDesc(),item.getQty(),item.getPrice());
                itemDTOList.add(itemDTO);
            }
            return itemDTOList;
        }
        return null;
    }

    //find by item using itemType id
    @Override
    public List<ItemDTO> findByItemType(int itemTypeId) throws Exception {
        List<Item>itemList=itemDAO.findByItemType(itemTypeId);
        if(itemList!=null){
            List<ItemDTO>itemDTOList=new ArrayList<>();
            for(Item item:itemList){
                ItemDTO itemDTO=new ItemDTO(item.getItemId(),item.getItemTypeId(),item.getItemDesc(),item.getQty(),item.getPrice());
                itemDTOList.add(itemDTO);
            }
            return itemDTOList;
        }
        return null;
    }

    @Override
    public boolean updateQty(int itemTypeId,int qty) throws Exception {
        Item item=new Item();
        item.setItemId(itemTypeId);
        item.setQty(qty);
        return itemDAO.updateQty(itemTypeId,qty);
    }
}
