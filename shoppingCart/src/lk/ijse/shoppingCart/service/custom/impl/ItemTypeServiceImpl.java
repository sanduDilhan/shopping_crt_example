package lk.ijse.shoppingCart.service.custom.impl;

import lk.ijse.shoppingCart.dao.DAOFactory;
import lk.ijse.shoppingCart.dao.custom.ItemTypeDAO;
import lk.ijse.shoppingCart.dto.ItemDTO;
import lk.ijse.shoppingCart.dto.ItemTypeDTO;
import lk.ijse.shoppingCart.model.Item;
import lk.ijse.shoppingCart.model.ItemType;
import lk.ijse.shoppingCart.service.custom.ItemTypeService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public class ItemTypeServiceImpl implements ItemTypeService {
    private ItemTypeDAO itemTypeDAO;

    public ItemTypeServiceImpl() throws Exception {
        itemTypeDAO= (ItemTypeDAO) DAOFactory.getInstance().getDAOFactory(DAOFactory.DAOType.ITEMTYPE);
    }

    @Override
    public boolean add(ItemTypeDTO itemTypeDto) throws Exception {
        ItemType itemType=new ItemType();
        itemType.setItemTypeId(itemTypeDto.getItemTypeId());
        itemType.setItemName(itemTypeDto.getItemName());
        return itemTypeDAO.add(itemType);
    }

    @Override
    public boolean update(ItemTypeDTO itemTypeDto) throws Exception {
        ItemType itemType=new ItemType();
        itemType.setItemTypeId(itemTypeDto.getItemTypeId());
        itemType.setItemName(itemTypeDto.getItemName());
        return itemTypeDAO.update(itemType);
    }

    @Override
    public boolean delete(String desc) throws Exception {
        boolean isDeleted=itemTypeDAO.delete(desc);
        return isDeleted;
    }

    @Override
    public ItemTypeDTO search(String name) throws Exception {
        ItemType itemType=itemTypeDAO.search(name);
        if(itemType!=null){
            ItemTypeDTO itemTypeDTO=new ItemTypeDTO();
            itemTypeDTO.setItemTypeId(itemType.getItemTypeId());
            itemTypeDTO.setItemName(itemType.getItemName());
            return itemTypeDTO;
        }
        return null;
    }

    @Override
    public List<ItemTypeDTO> findAll() throws Exception {
        List<ItemType> itemTypeList=itemTypeDAO.findAll();
        if(itemTypeList!=null){
            List<ItemTypeDTO> itemTypeDtoList=new ArrayList<>();
            for(ItemType itemType:itemTypeList){
                ItemTypeDTO itemTypeDTO=new ItemTypeDTO(itemType.getItemTypeId(),itemType.getItemName());
                itemTypeDtoList.add(itemTypeDTO);
            }
            return itemTypeDtoList;
        }
        return null;
    }

    @Override
    public ItemTypeDTO findByItemType(int id) throws Exception {
        ItemType itemType=itemTypeDAO.findByItemType(id);
        if(itemType!=null){
            ItemTypeDTO itemTypeDTO=new ItemTypeDTO();
            itemTypeDTO.setItemTypeId(itemType.getItemTypeId());
            itemTypeDTO.setItemName(itemType.getItemName());
            return itemTypeDTO;
        }
        return null;
    }
}
