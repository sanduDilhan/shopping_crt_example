package lk.ijse.shoppingCart.service.custom;

import lk.ijse.shoppingCart.dto.ItemTypeDTO;
import lk.ijse.shoppingCart.service.SuperService;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public interface ItemTypeService extends SuperService<ItemTypeDTO,String> {

    public ItemTypeDTO findByItemType(int id)throws Exception;
}
