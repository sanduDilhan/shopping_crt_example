package lk.ijse.shoppingCart.service.custom;

import lk.ijse.shoppingCart.dto.SuperAdminDTO;
import lk.ijse.shoppingCart.service.SuperService;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public interface SuperAdminService extends SuperService<SuperAdminDTO,String> {
    public SuperAdminDTO confirmPassword(String name, String password)throws Exception;
}
