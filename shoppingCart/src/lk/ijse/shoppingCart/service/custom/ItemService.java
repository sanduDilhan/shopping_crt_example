package lk.ijse.shoppingCart.service.custom;

import lk.ijse.shoppingCart.dto.ItemDTO;
import lk.ijse.shoppingCart.service.SuperService;

import java.util.List;

/**
 * Created by sandunDilhan on 6/16/2018.
 */
public interface ItemService extends SuperService<ItemDTO,String> {
    public List<ItemDTO> findByItemType(int itemTypeId)throws Exception;

    public boolean updateQty(int itemTypeId,int qty)throws Exception;
}
