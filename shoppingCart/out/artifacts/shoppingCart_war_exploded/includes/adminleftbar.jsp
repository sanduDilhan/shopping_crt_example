<%@ page import="lk.ijse.shoppingCart.dto.SuperAdminDTO" %>
Created by IntelliJ IDEA.
  User: Reshan
  Date: 06/04/2018
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="assest/images/use.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    <%out.println(((SuperAdminDTO)session.getAttribute("superAdmin")).getName());%>
                </p>
                <a href="#"><i class="fa fa-circle text-success"></i>(Admin)</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <%--<input type="text" name="q" class="form-control" placeholder="Search...">--%>
                <%--<span class="input-group-btn">--%>
                <%--<button type="submit" name="search" id="search-btn" class="btn btn-flat">--%>
                    <%--<i class="fa fa-search"></i>--%>
                <%--</button>--%>
              <%--</span>--%>
            </div>
        </form>

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="adminDashboard.jsp">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
              <!--<small class="label pull-right bg-green">new</small>-->
            </span>
                </a>
            </li>
            <!--.........................-->
            <li>
                <a href="adminManageSchool.jsp">
                    <i class="fa fa-bank"></i> <span>School</span>
                    <span class="pull-right-container">
              <!--<small class="label pull-right bg-green">new</small>-->
            </span>
                </a>
            </li>
            <!--..................................-->
            <li>
                <a href="adminManageUser.jsp">
                    <i class="ion ion-person-add"></i> <span>Request</span>
                    <span class="pull-right-container">
              <!--<small class="label pull-right bg-green">new</small>-->
            </span>
                </a>
            </li>
            <!--.....................-->
            <li>
                <a href="adminViewUserDetails.jsp">
                    <i class="fa fa-users"></i> <span>Users Details</span>
                    <span class="pull-right-container">
              <!--<small class="label pull-right bg-green">new</small>-->
            </span>
                </a>
            </li>
            <!--....................................-->
            <li>
                <a href="adminRejectRequest.jsp">
                    <i class="fa fa-remove"></i> <span>Reject Request</span>
                    <span class="pull-right-container">
              <!--<small class="label pull-right bg-green">new</small>-->
            </span>
                </a>
            </li>

            <!--.............................................-->


            <li class="treeview active" style="cursor: hand">
                <a target="#">
                    <i class="fa fa-paper-plane" ></i> <span>Message</span>
                    <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <%--<li id="vv"><a href="CreateMessage.jsp"><i class="fa fa-plus"></i>Create New Entry</a></li>--%>
                    <li><a href="../teacherSMS.jsp"><i class="fa fa-envelope-open"></i>Teacher</a></li>
                    <li><a href="../parentSMS.jsp"><i class="fa fa-envelope-open"></i>Parent</a></li>
                </ul>
            </li>

            <!--.............................................-->




            <li>
                <a href="/log_out">
                    <i class="fa fa-power-off"></i> <span>Logout</span>
                    <span class="pull-right-container">
              <!--<small class="label pull-right bg-green">new</small>-->
            </span>
                </a>
            </li>




        </ul>
    </section>
    <!-- /.sidebar -->
</aside>