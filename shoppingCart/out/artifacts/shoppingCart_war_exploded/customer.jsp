<%@ page import="lk.ijse.shoppingCart.service.custom.ItemTypeService" %>
<%@ page import="lk.ijse.shoppingCart.service.ServiceFactory" %>
<%@ page import="java.util.List" %>
<%@ page import="lk.ijse.shoppingCart.dto.ItemTypeDTO" %>
<%@ page import="lk.ijse.shoppingCart.dto.ItemDTO" %><%--
  Created by IntelliJ IDEA.
  User: sandunDilhan
  Date: 6/16/2018
  Time: 7:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if (session.getAttribute("loginResult") == null || session.getAttribute("loginResult").equals("false")) {
        response.sendRedirect("login.jsp");
        return;
    }
%>
<html>
<head>
    <title>PATpatSHOPPING.lk</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<style>
    body {
        min-height: 2000px;
        padding-top: 60px;
    }

    .navbar {
        position: fixed;
        top: 0;
        width: 100%;
        z-index: 500;
    }
</style>
</head>
<script>
    function selectItemType(){
        var e = document.getElementById("cmbItemType");
        var strUser = e.options[e.selectedIndex].text;
        var xmlhttp = new XMLHttpRequest();
        var url = "LoadItemHandler?itemTypeName=" + strUser;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                $("#tblItem td").remove();
                var obj = JSON.parse(xmlhttp.responseText);
                for(var i=0;i<obj.length;i++){
                    $("#tblItem").append("<tr>" +
                        "<td class='table-primary'>"+obj[i].itemId+"</td>" +
                        "<td class='table-secondary'>"+obj[i].itemDesc+"</td>" +
                        "<td class='table-success'>"+obj[i].qty+"</td>" +
                        "<td class='table-danger'>"+obj[i].price+"</td>" +
                        "<td>" +
                        "<button type='submit' class='btn btn-outline-danger' onclick='addToCart()' style='width: 120px ; border-radius: 15px'>add to cart</button>"+
                        " "+
                        "<button type='submit' class='btn btn-outline-warning' onclick='buyItem()' style='width: 25% ; border-radius: 15px'>buy</button>"+
                        "</td>" +
                        "</tr>");
                }
            }
        };
        try {
            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        } catch (e) {
            alert("unable to connect to server");
        }
    }
    
    function buyItem() {
        var table = document.getElementById("tblItem");
        for (var i = 1; i < table.rows.length; i++) {
            table.rows[i].onclick = function () {
                //rIndex = this.rowIndex;
                var xmlhttp = new XMLHttpRequest();
                var id = this.cells[0].innerHTML;
                var itemName = this.cells[1].innerHTML;
                var qty = this.cells[2].innerHTML;
                var rst = confirm("Are you sure... you want to buy "+itemName+"?");
                if (rst == true) {
                    var url = "UpdateItemQtyHandler?itemId=" + id+"&qty="+qty;
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if(xmlhttp.responseText==="update success"){
                                window.location.href="customer.jsp";
                            }else{
                                alert(xmlhttp.responseText);
                            }
                        }
                    };
                    try {
                        xmlhttp.open("GET", url, true);
                        xmlhttp.send();
                    } catch (e) {
                        alert("unable to connect to server");
                    }
                }
            };
        }
    }

    function addToCart() {
        var table = document.getElementById("tblItem");
        var tableCart = document.getElementById("tblCart");
        for (var i = 1; i < table.rows.length; i++) {
            table.rows[i].onclick = function () {
                var xmlhttp = new XMLHttpRequest();
                var itemName = this.cells[1].innerHTML;
                var qty = 1;
                var price = this.cells[3].innerHTML;

                var row = tableCart.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                cell1.innerHTML = itemName;
                cell2.innerHTML = "<input type='number' min='0' class='form-control' onkeyup='updateAmount()' id='tblTxtQty' name='tblQty' aria-describedby='emailHelp' style='border-radius: 10px;width: 60px' value="+qty+">";
                cell3.innerHTML = price;
                cell4.innerHTML="<button type='submit' class='btn btn-outline-danger' onclick='removeItem()' style='width: 120px ; border-radius: 15px'>remove</button>"

                var amount= $("#tblCart tr:last-child td:last-child").html();
                var price2=+amount + +price;
                $("#tblCart tr:last-child td:last-child").html(price2);
            };
        }
    }
    
    function removeItem() {
        var index, table = document.getElementById('tblCart');
        for(var i = 1; i < table.rows.length; i++)
        {
            table.rows[i].cells[3].onclick = function()
            {
                var c = confirm("do you want to delete this row");
                if(c === true)
                {
                    index = this.parentElement.rowIndex;
                    table.deleteRow(index);
                }
            };

        }
    }

    function updateAmount() {
        var table = document.getElementById("tblCart");
        for (var i = 1; i < table.rows.length; i++) {
            table.rows[i].onkeyup = function () {
                //rIndex = this.rowIndex;
                var price = this.cells[2].innerHTML;
                var qty = this.cells[1].querySelector("#tblTxtQty").value;

                var amount= $("#tblCart tr:last-child td:last-child").html();
                var price3=price*qty;
                var price2=(+amount + +price3)-price;
                $("#tblCart tr:last-child td:last-child").html(price2);

            };
        }
    }

    function buyAllItem() {
        var c = confirm("do you want to buy all time?");
        if(c === true){
            $("#tblCart").find("tr:gt(0)").remove();
        }
        alert("thank you...!!!")
    }
</script>
<body>
<div class="navbar">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <%--<li class="nav-item dropdown">--%>
                        <%--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--%>
                            <%--category--%>
                        <%--</a>--%>
                        <%--<div class="dropdown-menu" aria-labelledby="navbarDropdown" id="drpDwnItemType" onclick="selectItemType()">--%>
                        <select id="cmbItemType" onchange="selectItemType()">
                            <%
                                ItemTypeService itemTypeService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
                                List<ItemTypeDTO>itemTypeDTOList=itemTypeService.findAll();
                                for(ItemTypeDTO itemTypeDTO:itemTypeDTOList){
                                    %>
                            <%--<a class="dropdown-item"><%=itemTypeDTO.getItemName()%></a>--%>
                            <option><%=itemTypeDTO.getItemName()%></option>
                            <%
                                }
                            %>
                        </select>
                    <%--</div>--%>
                    <%--</li>--%>
                    <li class="nav-item">
                        <a class="nav-link disabled">settings</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <a href="login.jsp"><button class="btn btn-outline-danger my-2 my-sm-0" type="button">logout</button>
                </form>
            </div>
        </nav>
</div>
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active" style="height: 400px">
            <img class="d-block w-100" src="assest/images/google-shopping-holiday-gifts-ss-1920-800x450.jpg" alt="First slide">
        </div>
        <div class="carousel-item" style="height: 400px">
            <img class="d-block w-100" src="assest/images/computer.jpg" alt="Second slide">
        </div>
        <div class="carousel-item" style="height: 400px">
            <img class="d-block w-100" src="assest/images/shopping-bags-800.jpg" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div style="height: 50px">

</div>
<aside>
    <table class="table table-bordered" id="tblItem" style="width: 800px">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Item</th>
            <th scope="col">Qty On Hand</th>
            <th scope="col">price</th>
        </tr>
        </thead>
        <tbody>
        
        </tbody>
    </table>
</aside>
<aside style="float:right">
    <div>
        <table class="table table-bordered" id="tblCart" style="width: 525px">
            <thead>
            <tr>
                <th scope="col">Item</th>
                <th scope="col">Qty</th>
                <th scope="col">price</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="bg-primary">
                    <button type="submit" class="btn btn-outline-warning" onclick="buyAllItem()" style="width: 120px ; border-radius: 15px">buy</button>
                </td>
                <td id="txtAmount">0</td>
            </tr>
            </tbody>
        </table>
    </div>
</aside>
</body>
</html>
