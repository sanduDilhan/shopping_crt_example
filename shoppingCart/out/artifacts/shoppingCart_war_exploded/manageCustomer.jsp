<%@ page import="lk.ijse.shoppingCart.service.custom.CustomerService" %>
<%@ page import="lk.ijse.shoppingCart.service.ServiceFactory" %>
<%@ page import="java.util.List" %>
<%@ page import="lk.ijse.shoppingCart.dto.CustomerDTO" %><%--
  Created by IntelliJ IDEA.
  User: sandunDilhan
  Date: 6/17/2018
  Time: 2:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if (session.getAttribute("loginResult") == null || session.getAttribute("loginResult").equals("false")) {
        response.sendRedirect("adminLogin.jsp");
        return;
    }
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <title>PATpatSHOPPING.lk || adminManageCustomer</title>
</head>

<script>
    function RemoveCustomer() {
        var table = document.getElementById("tblCustomer");
        for (var i = 1; i < table.rows.length; i++) {
            table.rows[i].onclick = function () {
                //rIndex = this.rowIndex;
                var xmlhttp = new XMLHttpRequest();
                var customerName = this.cells[1].innerHTML;
                var rst = confirm("Are you sure... you want to REMOVE this customer?");
                if (rst == true) {
                    var url = "RemoveCustomerHandler?customerName=" + customerName;
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if(xmlhttp.responseText==="customer remove success"){
                                window.location.href="manageCustomer.jsp";
                            }else{
                                alert(xmlhttp.responseText);
                            }
                        }
                    };
                    try {
                        xmlhttp.open("GET", url, true);
                        xmlhttp.send();
                    } catch (e) {
                        alert("unable to connect to server");
                    }
                }
            };
        }
    }
</script>
<body style="background-color: #d2cbcb;height: 100px">
<div class="container">
    <br/>

    <h1 style="color: greenyellow;font-family: fantasy">Management Customer</h1>
    <br/><br/>
    <div class="row">
        <div class="col-sm-6">
            <form action="/addCustomer" method="post">
                <div class="form-group">
                    <label for="txtCustomerName">Customer Name</label>
                    <input type="text" class="form-control" id="txtCustomerName" name="customerName" aria-describedby="emailHelp" placeholder="Customer Name" style="border-radius: 15px">
                </div>
                <div class="form-group">
                    <label for="txtMobileNumber">Mobile Number</label>
                    <input type="text" maxlength="10" class="form-control" id="txtMobileNumber" name="MobileNumber" aria-describedby="emailHelp" placeholder="Mobile Number" style="border-radius: 15px">
                </div>
                <div class="form-group">
                    <label for="txtCreatePassword">create password</label>
                    <input type="password" class="form-control" id="txtCreatePassword" name="createPassword" aria-describedby="emailHelp" placeholder="Create Password" style="border-radius: 15px">
                </div>
                <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                <button type="submit" class="btn btn-outline-primary" style="width: 20% ; border-radius: 15px">Create</button>
                <a href="index.jsp"><button href="index.jsp" type="button" class="btn btn-link" style="width: 20% ; border-radius: 15px">Home</button></a>
            </form>
        </div>
        <div class="col-sm-6">
        </div>
    </div>
    <table id="tblCustomer" class="table table-dark">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Customer Name</th>
            <th scope="col">Mobile Number</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <%
            CustomerService customerService= (CustomerService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.CUSTOMER);
            //get all customer
            List<CustomerDTO>customerList=customerService.findAll();
            for(CustomerDTO customer:customerList){
        %>
        <tr>
            <td><%=customer.getCustomerId()%></td>
            <td><%=customer.getCustomerName()%></td>
            <td><%=customer.getMobileNumber()%></td>
            <td>
                <button type="submit" class="btn btn-outline-danger" onclick="RemoveCustomer()" style="width: 60% ; border-radius: 15px">Remove</button>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</div>
</body>
</html>
