<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>E</b>M</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Edu-Mo</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <!--<div class="navbar-custom-menu">-->
        <!--<ul class="nav navbar-nav">-->
        <!--&lt;!&ndash; Messages: style can be found in dropdown.less&ndash;&gt;-->
        <!--<li class="dropdown messages-menu">-->
        <!--<ul class="dropdown-menu">-->
        <!--<li class="header">You have 4 messages</li>-->
        <!--<li>-->
        <!--&lt;!&ndash; inner menu: contains the actual data &ndash;&gt;-->
        <!--<ul class="menu">-->
        <!--<li>&lt;!&ndash; start message &ndash;&gt;-->
        <!--<a href="#">-->
        <!--<div class="pull-left">-->
        <!--<img src="../../assest/images/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <!--</div>-->
        <!--<h4>-->
        <!--Support Team-->
        <!--<small><i class="fa fa-clock-o"></i> 5 mins</small>-->
        <!--</h4>-->
        <!--<p>Why not buy a new awesome theme?</p>-->
        <!--</a>-->
        <!--</li>-->
        <!--&lt;!&ndash; end message &ndash;&gt;-->
        <!--<li>-->
        <!--<a href="#">-->
        <!--<div class="pull-left">-->
        <!--<img src="../../assest/images/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <!--</div>-->
        <!--<h4>-->
        <!--AdminLTE Design Team-->
        <!--<small><i class="fa fa-clock-o"></i> 2 hours</small>-->
        <!--</h4>-->
        <!--<p>Why not buy a new awesome theme?</p>-->
        <!--</a>-->
        <!--</li>-->
        <!--<li>-->
        <!--<a href="#">-->
        <!--<div class="pull-left">-->
        <!--<img src="../../assest/images/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <!--</div>-->
        <!--<h4>-->
        <!--Developers-->
        <!--<small><i class="fa fa-clock-o"></i> Today</small>-->
        <!--</h4>-->
        <!--<p>Why not buy a new awesome theme?</p>-->
        <!--</a>-->
        <!--</li>-->
        <!--<li>-->
        <!--<a href="#">-->
        <!--<div class="pull-left">-->
        <!--<img src="../../assest/images/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <!--</div>-->
        <!--<h4>-->
        <!--Sales Department-->
        <!--<small><i class="fa fa-clock-o"></i> Yesterday</small>-->
        <!--</h4>-->
        <!--<p>Why not buy a new awesome theme?</p>-->
        <!--</a>-->
        <!--</li>-->
        <!--<li>-->
        <!--<a href="#">-->
        <!--<div class="pull-left">-->
        <!--<img src="../../assest/images/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <!--</div>-->
        <!--<h4>-->
        <!--Reviewers-->
        <!--<small><i class="fa fa-clock-o"></i> 2 days</small>-->
        <!--</h4>-->
        <!--<p>Why not buy a new awesome theme?</p>-->
        <!--</a>-->
        <!--</li>-->
        <!--</ul>-->
        <!--</li>-->
        <!--<li class="footer"><a href="#">See All Messages</a></li>-->
        <!--</ul>-->
        <!--</li>-->
        <!--&lt;!&ndash; Notifications: style can be found in dropdown.less &ndash;&gt;-->

        <!--&lt;!&ndash; Tasks: style can be found in dropdown.less &ndash;&gt;-->
        <!--&lt;!&ndash; User Account: style can be found in dropdown.less &ndash;&gt;-->
        <!--&lt;!&ndash;<li class="dropdown user user-menu">&ndash;&gt;-->
        <!--&lt;!&ndash;<a href="#" class="dropdown-toggle" data-toggle="dropdown">&ndash;&gt;-->
        <!--&lt;!&ndash;<img src="../../assest/images/user2-160x160.jpg" class="user-image" alt="User Image">&ndash;&gt;-->
        <!--&lt;!&ndash;<span class="hidden-xs">Reshan Maduka</span>&ndash;&gt;-->
        <!--&lt;!&ndash;</a>&ndash;&gt;-->
        <!--&lt;!&ndash;<ul class="dropdown-menu">&ndash;&gt;-->
        <!--&lt;!&ndash;&lt;!&ndash; User image &ndash;&gt;&ndash;&gt;-->
        <!--&lt;!&ndash;<li class="user-header">&ndash;&gt;-->
        <!--&lt;!&ndash;<img src="../../assest/images/user2-160x160.jpg" class="img-circle" alt="User Image">&ndash;&gt;-->

        <!--&lt;!&ndash;<p>&ndash;&gt;-->
        <!--&lt;!&ndash;Reshan Maduka - Web Developer&ndash;&gt;-->
        <!--&lt;!&ndash;<small>Member since Nov. 2017</small>&ndash;&gt;-->
        <!--&lt;!&ndash;</p>&ndash;&gt;-->
        <!--&lt;!&ndash;</li>&ndash;&gt;-->
        <!--&lt;!&ndash; Menu Body &ndash;&gt;-->
        <!--&lt;!&ndash;<li class="user-body">&ndash;&gt;-->
        <!--&lt;!&ndash;<div class="row">&ndash;&gt;-->
        <!--&lt;!&ndash;<div class="col-xs-4 text-center">&ndash;&gt;-->
        <!--&lt;!&ndash;<a href="#">Followers</a>&ndash;&gt;-->
        <!--&lt;!&ndash;</div>&ndash;&gt;-->
        <!--&lt;!&ndash;<div class="col-xs-4 text-center">&ndash;&gt;-->
        <!--&lt;!&ndash;<a href="#">Sales</a>&ndash;&gt;-->
        <!--&lt;!&ndash;</div>&ndash;&gt;-->
        <!--&lt;!&ndash;<div class="col-xs-4 text-center">&ndash;&gt;-->
        <!--&lt;!&ndash;<a href="#">Friends</a>&ndash;&gt;-->
        <!--&lt;!&ndash;</div>&ndash;&gt;-->
        <!--&lt;!&ndash;</div>&ndash;&gt;-->
        <!--&lt;!&ndash;&lt;!&ndash; /.row &ndash;&gt;&ndash;&gt;-->
        <!--&lt;!&ndash;</li>&ndash;&gt;-->
        <!--&lt;!&ndash; Menu Footer&ndash;&gt;-->
        <!--<li class="user-footer">-->
        <!--<div class="pull-left">-->
        <!--<a href="#" class="btn btn-default btn-flat">Profile</a>-->
        <!--</div>-->
        <!--<div class="pull-right">-->
        <!--<a href="#" class="btn btn-default btn-flat">Sign out</a>-->
        <!--</div>-->
        <!--</li>-->
        <!--</ul>-->
        <!--</li>-->
        <!--&lt;!&ndash; Control Sidebar Toggle Button &ndash;&gt;-->
        <!--&lt;!&ndash;<li>&ndash;&gt;-->
        <!--&lt;!&ndash;<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>&ndash;&gt;-->
        <!--&lt;!&ndash;</li>&ndash;&gt;-->
        <!--</ul>-->
        <!--</div>-->
    </nav>
</header>