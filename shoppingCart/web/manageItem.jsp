<%@ page import="lk.ijse.shoppingCart.service.custom.ItemTypeService" %>
<%@ page import="lk.ijse.shoppingCart.service.ServiceFactory" %>
<%@ page import="lk.ijse.shoppingCart.dto.ItemTypeDTO" %>
<%@ page import="java.util.List" %>
<%@ page import="lk.ijse.shoppingCart.service.custom.ItemService" %>
<%@ page import="lk.ijse.shoppingCart.dto.ItemDTO" %><%--
  Created by IntelliJ IDEA.
  User: sandunDilhan
  Date: 6/17/2018
  Time: 5:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if (session.getAttribute("loginResult") == null || session.getAttribute("loginResult").equals("false")) {
        response.sendRedirect("adminLogin.jsp");
        return;
    }
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <title>PATpatSHOPPING.lk || adminManageItem</title>
</head>
<script>
    function RemoveItem() {
        var table = document.getElementById("tblItem");
        for (var i = 1; i < table.rows.length; i++) {
            table.rows[i].onclick = function () {
                //rIndex = this.rowIndex;
                var xmlhttp = new XMLHttpRequest();
                var desc = this.cells[2].querySelector("#tblTxtDesc").value;
                var rst = confirm("Are you sure... you want to REMOVE this Item?");
                if (rst == true) {
                    var url = "RemoveItemHandler?description=" + desc;
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if(xmlhttp.responseText==="delete success"){
                                window.location.href="manageItem.jsp";
                            }else{
                                alert(xmlhttp.responseText);
                            }
                        }
                    };
                    try {
                        xmlhttp.open("GET", url, true);
                        xmlhttp.send();
                    } catch (e) {
                        alert("unable to connect to server");
                    }
                }
            };
        }
    }

    function updateItem() {
        var table = document.getElementById("tblItem");
        for (var i = 1; i < table.rows.length; i++) {
            table.rows[i].onclick = function () {
                //rIndex = this.rowIndex;
                var xmlhttp = new XMLHttpRequest();
                var id=this.cells[0].innerHTML;
                var itemTypeName=this.cells[1].innerHTML;
                var desc = this.cells[2].querySelector("#tblTxtDesc").value;
                var qty = this.cells[3].querySelector("#tblTxtQty").value;
                var price = this.cells[4].querySelector("#tblTxtPrice").value;
                var rst = confirm("Are you sure... you want to UPDATE this Item?");
                if (rst == true) {
                    var url = "UpdateItemHandler?itemId=" + id+"&itemTypeName="+itemTypeName+"&description="+desc+"&qty="+qty+"&price="+price;
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if(xmlhttp.responseText==="update success"){
                                window.location.href="manageItem.jsp";
                            }else{
                                alert(xmlhttp.responseText);
                            }
                        }
                    };
                    try {
                        xmlhttp.open("GET", url, true);
                        xmlhttp.send();
                    } catch (e) {
                        alert("unable to connect to server");
                    }
                }
            };
        }
    }

    $(document).keypress(
        function (event) {
            if (event.which == '13') {
                event.preventDefault();
            }


        });
</script>
<body style="background-color: #d2cbcb;height: 100px">
<div class="container">
    <br/>

    <h1 style="color: greenyellow;font-family: fantasy">Management Item</h1>
    <br/><br/>
    <div class="row">

        <div class="col-sm-6">
            <form action="/addItem" method="get">
                <div class="form-group">
                    <label for="cmbItemType">Item Type</label>
                    <select class="form-control" id="cmbItemType" name="itemTypeName" aria-describedby="emailHelp" style="border-radius: 15px">
                        <%
                            ItemTypeService itemTypeService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
                            //get All item type
                            List<ItemTypeDTO> itemTypeList=itemTypeService.findAll();
                            for(ItemTypeDTO itemType:itemTypeList) {
                            %>
                                <option><%=itemType.getItemName()%></option>
                        <%
                            }
                        %>
                    </select>

                </div>
                <div class="form-group">
                    <label for="txtDescription">Item Description</label>
                    <input type="text" class="form-control" id="txtDescription" name="description" aria-describedby="emailHelp" placeholder="Item Description" style="border-radius: 15px">
                </div>
                <div class="form-group">
                    <label for="txtQty">QTY</label>
                    <input type="number" min="0" class="form-control" id="txtQty" name="qty" aria-describedby="emailHelp" placeholder="QTY" style="border-radius: 15px">
                </div>
                <div class="form-group">
                    <label for="txtPrice">Price</label>
                    <input type="number" min="0" class="form-control" id="txtPrice" name="price" aria-describedby="emailHelp" placeholder="Price" style="border-radius: 15px">
                </div>
                <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                <button type="submit" class="btn btn-outline-primary" style="width: 20% ; border-radius: 15px">Create</button>
                <a href="index.jsp"><button href="index.jsp" type="button" class="btn btn-link" style="width: 20% ; border-radius: 15px">Home</button></a>
            </form>
        </div>
        <div class="col-sm-6">
        </div>
    </div>
    <table id="tblItem" class="table table-dark">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Item Type</th>
            <th scope="col">description</th>
            <th scope="col">QTY</th>
            <th scope="col">Price</th>
        </tr>
        </thead>
        <tbody>
        <%
            ItemService itemService= (ItemService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEM);
            ItemTypeService itemTService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
            //get All Item
            List<ItemDTO>itemList=itemService.findAll();
            for(ItemDTO item:itemList){
                ItemTypeDTO itemTypeDTO=itemTypeService.findByItemType(item.getItemTypeId());
             %>
        <tr>
            <td><%=item.getItemId()%></td>
            <td><%=itemTypeDTO.getItemName()%></td>
            <td><input type="text" class="form-control" id="tblTxtDesc" name="tblDesc" aria-describedby="emailHelp" style="border-radius: 10px;width: 250px" value="<%=item.getItemDesc()%>"></td>
            <td><input type="number" min="0" class="form-control" id="tblTxtQty" name="tblQty" aria-describedby="emailHelp" style="border-radius: 10px;width: 60px" value="<%=item.getQty()%>"></td>
            <td><input type="number" min="0" class="form-control" id="tblTxtPrice" name="tblPrice" aria-describedby="emailHelp" style="border-radius: 10px;width: 100px" value="<%=item.getPrice()%>"></td>
            <td width="400px">
                <button type="submit" class="btn btn-outline-success" onclick="updateItem()" style="width: 40% ; border-radius: 15px">Update</button>
                <button type="submit" class="btn btn-outline-danger" onclick="RemoveItem()" style="width: 40% ; border-radius: 15px">Remove</button>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</div>
</body>
</html>

