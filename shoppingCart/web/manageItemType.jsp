<%@ page import="lk.ijse.shoppingCart.service.custom.ItemTypeService" %>
<%@ page import="lk.ijse.shoppingCart.service.ServiceFactory" %>
<%@ page import="java.util.List" %>
<%@ page import="lk.ijse.shoppingCart.dto.ItemTypeDTO" %><%--
  Created by IntelliJ IDEA.
  User: sandunDilhan
  Date: 6/17/2018
  Time: 6:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if (session.getAttribute("loginResult") == null || session.getAttribute("loginResult").equals("false")) {
        response.sendRedirect("adminLogin.jsp");
        return;
    }
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

    <title>PATpatSHOPPING.lk || adminManageItemType</title>
</head>
<script>
    function RemoveItemType() {
        var table = document.getElementById("tblItemType");
        for (var i = 1; i < table.rows.length; i++) {
            table.rows[i].onclick = function () {
                //rIndex = this.rowIndex;
                var xmlhttp = new XMLHttpRequest();
                var desc = this.cells[1].innerHTML;
                var rst = confirm("Are you sure... you want to REMOVE this Item?");
                if (rst == true) {
                    var url = "RemoveItemTypeHandler?description=" + desc;
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if(xmlhttp.responseText==="remove success"){
                                window.location.href="manageItemType.jsp";
                            }else{
                                alert(xmlhttp.responseText);
                            }
                        }
                    };
                    try {
                        xmlhttp.open("GET", url, true);
                        xmlhttp.send();
                    } catch (e) {
                        alert("unable to connect to server");
                    }
                }
            };
        }
    }
</script>
<body style="background-color: #d2cbcb;height: 100px">
<div class="container">
    <br/>

    <h1 style="color: greenyellow;font-family: fantasy">Management ItemType</h1>
    <br/><br/>
    <div class="row">

        <div class="col-sm-6">
            <form action="/addItemType" method="get">
                <div class="form-group">
                    <label for="txtItemType">Item Type</label>
                    <input type="text" class="form-control" id="txtItemType" name="itemType" aria-describedby="emailHelp" placeholder="Item Type" style="border-radius: 15px">
                </div>
                <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                <button type="submit" class="btn btn-outline-primary" style="width: 20% ; border-radius: 15px">Create</button>
                <a href="index.jsp"><button href="index.jsp" type="button" class="btn btn-link" style="width: 20% ; border-radius: 15px">Home</button></a>
            </form>
        </div>
        <div class="col-sm-6">
        </div>
    </div>
    <table id="tblItemType" class="table table-dark">
        <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Item Type</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <%
            ItemTypeService itemTypeService= (ItemTypeService) ServiceFactory.getInstance().getServiceFactory(ServiceFactory.ServiceType.ITEMTYPE);
            //get All item type
            List<ItemTypeDTO>itemTypeList=itemTypeService.findAll();
            for(ItemTypeDTO itemType:itemTypeList){
                %>
        <tr>
            <td><%=itemType.getItemTypeId()%></td>
            <td><%=itemType.getItemName()%></td>
            <td>
            <button type="submit" onclick="RemoveItemType()" class="btn btn-outline-danger" style="width: 60% ; border-radius: 15px">Remove</button>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
</div>
</body>
</html>

