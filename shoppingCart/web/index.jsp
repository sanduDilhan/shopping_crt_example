<%--
  Created by IntelliJ IDEA.
  User: sandunDilhan
  Date: 6/13/2018
  Time: 3:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  if (session.getAttribute("loginResult") == null || session.getAttribute("loginResult").equals("false")) {
    response.sendRedirect("adminLogin.jsp");
    return;
  }
%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

  <title>PATpatSHOPPING.lk || adminDashboard</title>
</head>
<body style="background-color: #d2cbcb;height: 100px">
<div class="container">
  <br/>
  <center>
    <h1 style="color: greenyellow;font-family: fantasy">Dashboard</h1>
    <br/><br/>
    <div class="row">
      <div class="col-sm-12">
        <br/>

        <a href="manageCustomer.jsp"><button type="button" class="btn btn-outline-primary" style="width: 20% ; border-radius: 15px">Manage Customer</button></a>
        <br/> <br/>
        <a href="manageItem.jsp"><button type="button" class="btn btn-outline-secondary" style="width: 20% ; border-radius: 15px">Manage Item</button></a>
        <br/> <br/>
        <a href="manageItemType.jsp"><button type="button" class="btn btn-outline-light" style="width: 20% ; border-radius: 15px">Manage Item Type</button></a>
        <br/> <br/>
        <a href="adminLogin.jsp"><button type="button" class="btn btn-outline-danger" style="width: 20% ; border-radius: 15px">LogOut</button></a>
      </div>
    </div>
  </center>
</div>
</body>
</html>
